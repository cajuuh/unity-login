﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using BestHTTP;
using BestHTTP.Authentication;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Security.Cryptography;

public class Register : MonoBehaviour
{
    #region variables
    public GameObject UserName;
    public GameObject EMail;
    public GameObject Password;
    public GameObject ConfPassword;

    [SerializeField]
    private GameObject _registerButton;

    private string _username;
    private string _email;
    private string _password;
    private string _confPassword;
    private string _form;
    private bool _isEmailValid = false;
    static readonly string PasswordHash = "T0p1cPl4yG4m35";
    static readonly string SaltKey = " K%43ajs$#@2())maknsdjk%¨$$¨5";
    static readonly string VIKey = "@1B2c3D4e5F6g7H8";

    protected static IMongoClient _client1;
    protected static IMongoDatabase _database;
    #endregion

    void Start()
    {
        if (!Directory.Exists("Users"))
        {
            Directory.CreateDirectory("Users");
        }

        var settings = new MongoClientSettings
        {
            Server = new MongoServerAddress("35.198.4.42", 27017),
            UseSsl = false
        };

        _client1 = new MongoClient(settings);

        //simple get request
        //TODO remove when encrypt decrypt appears to work fine
        //HTTPRequest request = new HTTPRequest(new Uri("https://google.com"), OnRequestFinished);
        //request.Send();
    }

    void Update()
    {
        //setting variables
        _username = UserName.GetComponent<InputField>().text;
        _email = EMail.GetComponent<InputField>().text;
        _password = Password.GetComponent<InputField>().text;
        _confPassword = ConfPassword.GetComponent<InputField>().text;

        TabInput();
    }

    /// <summary>
    /// when tab is pressed change the field
    /// </summary>
    private void TabInput()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (UserName.GetComponent<InputField>().isFocused)
            {
                EMail.GetComponent<InputField>().Select();
            }
            if (EMail.GetComponent<InputField>().isFocused)
            {
                Password.GetComponent<InputField>().Select();
            }
            if (Password.GetComponent<InputField>().isFocused)
            {
                ConfPassword.GetComponent<InputField>().Select();
            }
            if (ConfPassword.GetComponent<InputField>().isFocused)
            {
                _registerButton.GetComponent<Button>().Select();
            }
        }

        //if everything is good call the function that'll be called by the button click
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            if (_password != "" && _email != "" && _username != "" && _confPassword != "")
            {
                ConfirmRegister();
            }
        }
    }

    /// <summary>
    /// analisys sum up the registration in every field taken
    /// </summary>
    public void ConfirmRegister()
    {
        //MongoDB initialization
        var collection = _database.GetCollection<BsonDocument>("Users");
        var userDoc = new BsonDocument();

        //booleans that'll set if the fields are or not validated.
        #region boolean fields
        bool uname = false;
        bool umail = false;
        bool upassword = false;
        bool uconfpassword = false;
        #endregion

        //username verification
        #region username verify
        if (_username != "")
        {
            if (!System.IO.File.Exists(path: Application.dataPath + "/Users/" + _username + ".txt"))
            {
                uname = true;
            }
            else
            {
                Debug.LogWarning("Username Taken.");
            }
        }
        else
        {
            Debug.LogWarning("Username Field Empty!");
        }
        #endregion

        //email verification
        #region email verify
        if (_email != "")
        {
            EmailValidation(_email);
            if (_isEmailValid && _email.Contains("@") && _email.Contains("."))
            {
                umail = true;
            }
            else
            {
                Debug.LogWarning("Invalid Email.");
            }
        }
        else
        {
            Debug.LogWarning("Email Field Empty!");
        }
        #endregion

        //password verification
        #region password verify
        if (_password != "")
        {
            if (_password.Length > 5)
            {
                upassword = true;
            }
            else
            {
                Debug.LogWarning("Password is too short.");
            }
        }
        else
        {
            Debug.LogWarning("Password Field Empty!");
        }
        #endregion

        //password confirmation verification
        #region password confirmation verify
        if (_confPassword != "")
        {
            if (_confPassword == _password)
            {
                uconfpassword = true;
            }
            else
            {
                Debug.LogWarning("Passwords don't match.");
            }
        }
        else
        {
            Debug.LogWarning("Confirmation Password Field Empty!");
        }
        #endregion

        //after all set up saves the user
        if (uname && umail && upassword && uconfpassword)
        {

            Encrypt(_password);

            //old local storage
            //TODO remove this code when the encrypt decrypt is working fine
            //_form = ("username: " + _username + "\r\n" + "e-mail: " + _email + "\r\n" + _password);
            //System.IO.File.WriteAllText(path: Application.dataPath + "/Users/" + _username + ".txt", contents: _form);

            userDoc.Add("Email", _email);
            userDoc.Add("Password", _password);

            #region clear input fields
            UserName.GetComponent<InputField>().text = "";
            EMail.GetComponent<InputField>().text = "";
            Password.GetComponent<InputField>().text = "";
            ConfPassword.GetComponent<InputField>().text = "";
            #endregion

            print("Registration complete!");
        }
    }

    void EmailValidation(string email)
    {
        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match match = regex.Match(email);
        if (match.Success)
        {
            _isEmailValid = true;
        }
        else
        {
            _isEmailValid = false;
        }
    }

    /// <summary>
    /// Best HTTP for use when POST and GET needed
    /// </summary>
    /// <param name="request">type and what will be asked for the server</param>
    /// <param name="response">the response expected by the client</param>
    void OnRequestFinished(HTTPRequest request, HTTPResponse response)
    {
        Debug.Log("Request Finished! Text received: " + response.DataAsText);
    }

    /// <summary>
    /// Encrypt the password using a combynation of cipher and utf
    /// </summary>
    /// <param name="plainText">the password in plaintext that'll be encrypted</param>
    /// <returns>the encrypted password</returns>
    private static string Encrypt(string plainText)
    {
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
        var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
        var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

        byte[] cipherTextBytes;

        using (var memoryStream = new MemoryStream())
        {
            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
            {
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();
                cipherTextBytes = memoryStream.ToArray();
                cryptoStream.Close();
            }
            memoryStream.Close();
        }
        return Convert.ToBase64String(cipherTextBytes);
    }
}
