﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using NUnit.Framework.Constraints;
using UnityEngine;
using UnityEngine.UI;
using BestHTTP;
using BestHTTP.Authentication;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Security.Cryptography;

public class Login : MonoBehaviour
{
    #region variables

    public GameObject UserName;
    public GameObject Password;
    public GameObject ConfirmImage;

    [SerializeField]
    private GameObject _loginButton;

    private string _username;
    private string _password;
    private string[] _lines;
    private string _decryptedP;

    //enc settings
    private static readonly string PasswordHash = ")(&987YPIUHijukBHoIUYt9876";
    private static readonly string SaltKey = "K%43ajs$#@2())maknsdjk%¨$$¨5";
    private static readonly string VIKey = "@1B2c3D4e5F6g7H8";


    protected static IMongoClient _client;
    private static IMongoDatabase _database;
    #endregion


    void Start()
    {
        var settings = new MongoClientSettings
        {
            Server = new MongoServerAddress("35.198.4.42", 27017),
            UseSsl = false
        };

        _client = new MongoClient(settings);
    }

    static async Task MongoASyncTask()
    {
        _database = _client.GetDatabase("topicplay");
        var collection = _database.GetCollection<BsonDocument>("Users");

        using (IAsyncCursor<BsonDocument> cursor = await collection.FindAsync(new BsonDocument()))
        {
            while (await cursor.MoveNextAsync())
            {
                IEnumerable<BsonDocument> batch = cursor.Current;
                foreach (BsonDocument document in batch)
                {
                    Console.WriteLine(document);
                    Console.WriteLine();
                }
            }
        }
    }

    void Update()
    {
        _username = UserName.GetComponent<InputField>().text;
        _password = Password.GetComponent<InputField>().text;

        if (UserName.GetComponent<InputField>().isFocused)
        {
            ConfirmImage.SetActive(false);
        }

        TabInput();
    }

    private void TabInput()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (UserName.GetComponent<InputField>().isFocused)
            {
                Password.GetComponent<InputField>().Select();
            }
            if (Password.GetComponent<InputField>().isFocused)
            {
                _loginButton.GetComponent<Button>().Select();
            }

        }

        //if everything is good call the function that'll be called by the button click
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            if (_password != "" && _username != "")
            {
                ConfirmLogin();
            }
        }
    }

    private void ConfirmLogin()
    {
        var collection = _database.GetCollection<BsonDocument>("Users");
        var userDoc = new BsonDocument();
        bool uname = false;
        bool upassword = false;

        if (_username != "")
        {
            if (System.IO.File.Exists(path: Application.dataPath + "/Users/" + _username + ".txt"))
            {
                uname = true;
                _lines = System.IO.File.ReadAllLines(Application.dataPath + "/Users/" + _username + ".txt");
            }
            else
            {
                Debug.LogWarning("Username taken.");
            }
        }
        else
        {
            Debug.LogWarning("Username Field Empty!");
        }

        if (_password != "")
        {
            if (System.IO.File.Exists(path: Application.dataPath + "/Users/" + _username + ".txt"))
            {
                var i = 1;
                foreach (var c in _lines[2])
                {
                    i++;
                    var decrypted = (char)(c / i);
                    _decryptedP += decrypted.ToString();
                }
                if (_password.Equals(_decryptedP))
                {
                    upassword = true;
                }
                else
                {
                    Debug.LogWarning(_decryptedP);
                }
            }
            else
            {
                Debug.LogWarning("Password Invalid.");
            }
        }
        else
        {
            Debug.LogWarning("Password Field Empty!");
        }

        if (uname == true && upassword == true)
        {
            UserName.GetComponent<InputField>().text = "";
            Password.GetComponent<InputField>().text = "";
            ConfirmImage.SetActive(true);
            print("login sucessful!");
        }
    }

    /// <summary>
    /// decrypt script for the password
    /// </summary>
    /// <param name="encryptedText">encrypted password</param>
    /// <returns>the decryoted password if needed</returns>
    private static string Decrypt(string encryptedText)
    {
        byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
        byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
        var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

        var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
        var memoryStream = new MemoryStream(cipherTextBytes);
        var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
        byte[] plainTextBytes = new byte[cipherTextBytes.Length];

        int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
        memoryStream.Close();
        cryptoStream.Close();
        return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
    }
}
