@{
	Author = 'Roman Kuzmin'
	ModuleVersion = '4.8.8'
	Description = 'Mdbc module - MongoDB Cmdlets for PowerShell'
	CompanyName = 'https://github.com/nightroman/Mdbc'
	Copyright = 'Copyright (c) 2011-2016 Roman Kuzmin'

	ModuleToProcess = 'Mdbc.dll'
	RequiredAssemblies = 'MongoDB.Driver.dll', 'MongoDB.Bson.dll'

	PowerShellVersion = '2.0'
	GUID = '12c81cd8-bde3-4c91-a292-e6c4f868106a'

	PrivateData = @{
		PSData = @{
			Tags = 'Mongo', 'MongoDB', 'Database'
			LicenseUri = 'http://www.apache.org/licenses/LICENSE-2.0'
			ProjectUri = 'https://github.com/nightroman/Mdbc'
			ReleaseNotes = 'https://github.com/nightroman/Mdbc/blob/master/Release-Notes.md'
		}
	}
}
